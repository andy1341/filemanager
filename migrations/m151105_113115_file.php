<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_113115_file extends Migration
{
    public function up()
    {
        $this->createTable('files',[
            'id'=>Schema::TYPE_PK,
            'basename'=>Schema::TYPE_TEXT . " NOT NULL",
            'extension'=>Schema::TYPE_TEXT . " NOT NULL",
            'dir_id'=>Schema::TYPE_INTEGER
        ]);

        $this->createTable('dirs',[
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_TEXT . " NOT NULL"
        ]);
    }

    public function down()
    {
        $this->dropTable('files');
        $this->dropTable('dirs');
        echo "m151105_113115_file cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
