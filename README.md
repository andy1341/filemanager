Nginx conf
=====================================
```code
server {
	    charset utf-8;
	    client_max_body_size 128M;

	    server_name filemanager.ll;
	    root        /var/www/filemanager/web;
	    index       index.php;

	    location / {
		# Redirect everything that isn't a real file to index.php
		try_files $uri $uri/ /index.php?$args;
		include	  mime.types;
	    }

#	     uncomment to avoid processing of calls to non-existing static files by Yii
	    location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
	        try_files $uri =404;
	    }
	    error_page 404 /404.html;

	    location ~ \.php$ {
		include fastcgi_params;
		fastcgi_pass unix:/tmp/php-fpm.sock;
		include fastcgi.conf;
		root /var/www/filemanager/web;
	    }

	    location ~ /\.(ht|svn|git) {
		deny all;
	    }
	}
	```

Настроить доступ к базе в config/db.php

php yii migrate - для создания таблиц