<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class UploadForm extends Model
{

    public $textFiles;

    public $newdir;

    public $uploaddir;

    const UPLOAD_DIR = '../uploads/';

    public function rules()
    {
        return [
            [['textFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => ['txt','doc'], 'maxFiles' => 4],
            [['uploaddir'], 'required', 'message' => "Выберете папку"],
            ['newdir','required', 'when' => [$this,'isNewDir'], 'whenClient' => $this->clientIsNewDir()],
            ['uploaddir', 'validateSpace',]
        ];
    }

    public function isNewDir($model) {
        return $model->uploaddir == 'new';
    }

    public function clientIsNewDir() {
        return "function (attribute, value) {
        console.log('dsds');
        return $('#uploadform-uploaddir :selected')[0].value == 'new';
    }";
    }

    public function validateSpace($attr)
    {
        $dir = $this->getUploaddir();

        $free = Dirs::FILES_IN_DIR - count($dir->files);

        if ($free < count($this->textFiles)) {
            $this->addError($attr, "В папке $dir осталось место для $free файлов ");
        }
    }

    protected function getUploaddir() {

        if (!$this->uploaddir instanceof Dirs) {

            if ($this->uploaddir == 'new') {

                $dir = new Dirs();
                $dir->name = $this->newdir;

                if (!$dir->save() && $dir->errors['name'][0] == 'exist') {

                    $dir = Dirs::findOne(['name'=>$this->newdir]);
                }
            } else {

                $dir = Dirs::findOne($this->uploaddir);

            }

            $this->uploaddir = $dir;

        }

        return $this->uploaddir;

    }

    public function upload()
    {

        if ($this->validate()) {

            $dir = $this->getUploaddir();

            foreach ($this->textFiles as $file) {

                $new_file = new Files();
                $new_file -> basename = $file->baseName;
                $new_file -> extension = $file->extension;
                $new_file -> dir_id = $dir -> id;
                $new_file -> save();


                $this->checkPath($dir->name);

                $file->saveAs("../uploads/$dir->name/$new_file->basename.$new_file->extension");

            }

            return true;

        } else {

            return false;

        }
    }

    protected function checkPath($path) {

        $result = FALSE;

        if (!file_exists(self::UPLOAD_DIR)) {
            mkdir(self::UPLOAD_DIR);
        }

        if (!file_exists(self::UPLOAD_DIR.$path)) {
            mkdir(self::UPLOAD_DIR.$path);
        }

        if (file_exists(self::UPLOAD_DIR.$path) && is_dir(self::UPLOAD_DIR.$path)) {
            $result = TRUE;
        }

        return $result;
    }

}