<?php

namespace app\models;

/**
 * This is the model class for table "posts".
 *
 * @property string $path
 * @property Dirs $dir
 * @property integer $id
 * @property string $title
 * @property string $data
 * @property string $create_time
 * @property string $update_time
 */
class Files extends \yii\db\ActiveRecord
{
	const UPLOAD_DIR = 'uploads';

	const FILE_NAME_REGEXP = '/(.*)\((\d+)\)/';
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'files';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				['basename', 'extension', 'dir_id'],
				'required',
				'message'=>'null'
			],
			[
				['basename','extension', 'dir_id'],
				'unique',
				'targetAttribute' => ['basename','extension', 'dir_id'],
				'message'=>'exist'
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'basename' => 'Basename',
			'extension' => 'Extension',
			'dir_id' => 'Dir ID'
		];
	}

	public function getDir() {
		return $this->hasOne(Dirs::className(),['id'=>'dir_id']);
	}

	public function __toString() {
		return $this->basename.'.'.$this->extension;
	}

	public function getPath() {
		return \Yii::$app->basePath . '/' . self::UPLOAD_DIR .'/'. $this->dir . "/" . $this->__toString();
	}

	public static function getUploadCatalog() {
		$dirs = Dirs::find()->all();
		$data =[];
		foreach ($dirs as $dir) {
			$data[$dir->name] = $dir->files;
		}

		return $data;
	}

	public function validate($attributeNames = null, $clearErrors = true) {

		$result = parent::validate($attributeNames,$clearErrors = true);

		if (!$result) {

			$error = array_values($this->errors)[0][0];

			if ($error == 'exist') {

				$this->basename = preg_match(self::FILE_NAME_REGEXP,$this->basename)
					? preg_replace_callback(self::FILE_NAME_REGEXP,array($this,'incFileIndex') , $this->basename)
					: $this -> basename . '(1)';

				$this -> save();

				$result = parent::validate();
			}
		}

		return $result;

	}

	protected function incFileIndex($matches) {

		$index =intval($matches[2]);

		return "$matches[1](". ++$index  .")";

	}
}
