<?php

namespace app\models;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $title
 * @property string $files
 * @property string $data
 * @property string $create_time
 * @property string $update_time
 */
class Dirs extends \yii\db\ActiveRecord
{
	const FILES_IN_DIR = 10;
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'dirs';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'unique', 'targetAttribute' => 'name','message'=>'exist'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Title',
		];
	}

	public function getFiles() {
		return $this->hasMany(Files::className(),['dir_id'=>'id']);
	}

	public function __toString() {
		return strval($this->name);
	}

	public static function getIncomplateDirs() {

		$dirs = Dirs::find()->all();

		$res = [];

		foreach ($dirs as $dir) {
			if (count($dir->files)<self::FILES_IN_DIR) {

				$res[$dir->id] = $dir;

			}
		}

		return $res;


	}
}
