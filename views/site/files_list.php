<?php
use yii\helpers\Html;
if (empty($data)) {

echo "<h3>Загруженых файлов нет</h3>";

} else {

foreach ($data as $dir => $files) {

$dir_href = Html::a('/' . $dir, null, ['class' => 'dir-href','name'=>$dir]) . "<br>";

$files_hrefs = [];

foreach ($files as $file) {
$filename="$dir/" . $file['basename']. '.' . $file['extension'];
$files_hrefs[] = Html::a($file['basename'] . "." . $file['extension'], null, ['class' => 'file-href','id'=>$file['id']]);
}

$files_container = Html::tag('div', implode('<br>',$files_hrefs),['class'=>'files-container','id'=>$dir]);
echo  $dir_href . $files_container;
}
}