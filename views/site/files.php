<?php
$this ->registerJsFile('js/list.js');
?>

<h1>Список файлов</h1>
<div class="dir-wrap">
<?php
include_once "files_list.php";
?>
</div>

<div class="overlay" title="окно"></div>
<div class="popup">
    <div class="close_window">x</div>
    <pre></pre>
</div>

<div class="action-popup">
    <a href="" action="show">Просмотреть</a>
    <a href="" action="delete">Удалить</a>
    <a href="" action="download">Скачать</a>
</div>
