<?php
use yii\widgets\ActiveForm;
$this ->registerJsFile('js/upload.js');

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'uploaddir')->dropDownList($dirs) ?>
<?= $form->field($model, 'newdir',['options'=>['class'=>'hidden']])->input('text')?>
<?= $form->field($model, 'textFiles[]')->fileInput(['multiple' => true]) ?>

    <button>Submit</button>

<?php ActiveForm::end() ?>