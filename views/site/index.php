<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Загрузчик файлов';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Добро пожаловать</h1>

        <p class="lead">Выберете что сделать</p>
        <p><?php echo Html::a('Загрузить файлы',array('site/upload'),array('class'=>"btn btn-lg btn-success"))?></p>
        <p><?php echo Html::a('Просмотреть файлы',array('site/files'),array('class'=>"btn btn-lg btn-success"))?></p>
    </div>

    <div class="body-content">



    </div>
</div>
