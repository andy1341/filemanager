/**
 * Created by an on 04.11.15.
 */
listLoad = function() {

    $('.file-href').click(function(e) {
        e.preventDefault();
        $('.action-popup')
            .css({
                'opacity': 1,
                'visibility': 'visible',
                'top': e.clientY + 10,
                'left': e.clientX+20
            })
            .attr('id',e.target.id);
        $('a[action=download]').attr('href','index.php?r=site/download&f='+ e.target.id);
        $('.overlay').css({'visibility':'visible'});
    });

    $('.dir-href').click(function(e) {
        e.preventDefault();
        var dir = e.target.name;
        $('#'+dir+'.files-container').toggle(400);
    });
};

window.onload=function() {

    $('.popup .close_window, .overlay').click(function (){
        $('.popup, .overlay, .action-popup').css({'opacity': 0, 'visibility': 'hidden'});
    });

    $('a[action=show]').click(function(e) {
        e.preventDefault();
        var file_id = e.target.parentElement.id;
        $.ajax({
            type: "GET",
            url: "index.php",
            data: "r=site/showfile&f="+file_id,
            success: function(msg){
                $('.popup>pre').html(msg);
                $('.popup, .overlay').css({'opacity': 1, 'visibility': 'visible'});
            }
        });
        $('.popup, .overlay, .action-popup').css({'opacity': 0, 'visibility': 'hidden'});

    });

    $('a[action=delete]').click(function(e) {
        e.preventDefault();

        var file_id = e.target.parentElement.id;
        $.ajax({
            type: "GET",
            url: "index.php",
            data: "r=site/deletefile&f="+file_id,
            success: function(msg){
                $('.dir-wrap').html(msg);
                listLoad();
            }
        });
        $('.popup, .overlay, .action-popup').css({'opacity': 0, 'visibility': 'hidden'});

    });

    $('a[action=download]').click(function(e) {
        $('.popup, .overlay, .action-popup').css({'opacity': 0, 'visibility': 'hidden'});

    });

    listLoad();
};