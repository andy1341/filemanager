/**
 * Created by an on 04.11.15.
 */
window.onload = function() {

    var toggle = function () {
        var elems = {
            'newDirInput' : '.field-uploadform-newdir',
            'dirSelectValue': '#uploadform-uploaddir :selected'
        };

        if ($(elems.dirSelectValue).attr('value') == 'new') {
            $(elems.newDirInput).removeClass('hidden');
        } else {
            $(elems.newDirInput).addClass('hidden');
        }
    };

    toggle();

    $('#uploadform-uploaddir').change(toggle);
};