<?php

namespace app\controllers;

use app\models\CForm;
use app\models\Dirs;
use app\models\Files;
use app\models\UploadForm;
use Faker\Provider\File;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\UploadedFile;

class SiteController extends Controller
{

    const FILE_PATH_REGEXP = '/(.*)\/(.*)\.(.*)/';

    const FILE_NOT_FOUND_MESSAGE = 'Файл не найдет!';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionFiles() {
        return $this->render('files',['data'=>Files::getUploadCatalog()]);
    }

    public function actionUpload() {

        $model = new UploadForm();

        if (Yii::$app->request->isPost) {

            $model -> load(Yii::$app->request->post());

            $model -> textFiles = UploadedFile::getInstances($model, 'textFiles');

            if ($model -> upload()) {

                return Yii::$app->getResponse() -> redirect(['site/files']);

            }
        }

        $dirs = ArrayHelper::map(Dirs::getIncomplateDirs(),'id','name');

        $dirs['new']= 'Новая папка';

        return $this->render('upload', ['model' => $model,'dirs'=>$dirs]);
    }

    public function actionShowfile() {

        $request = Yii::$app -> getRequest();

        if ($request->isAjax) {


            $file = Files::findOne($request -> get('f'));

            if ($file instanceof Files && is_file($file->path)) {

                return file_get_contents($file->path);

            } else {

                return self::FILE_NOT_FOUND_MESSAGE;
            }


        } else {

            return Yii::$app -> getResponse() -> redirect('index.php');

        }
    }

    public function actionDeletefile() {

        $request = Yii::$app->getRequest();

        if ($request->isAjax) {

            $file = Files::findOne($request -> get('f'));

            if ($file instanceof Files && is_file($file->path)) {

                unlink($file->path);

                $file->delete();

                $answer = $this->renderAjax('files_list', ['data'=>Files::getUploadCatalog()]);

            } else {

                $answer = self::FILE_NOT_FOUND_MESSAGE;
            }

            return $answer;

        } else {

            return Yii::$app -> getResponse() -> redirect('index.php');

        }
    }

    public function actionDownload() {

        $request = Yii::$app->request;

        $file = Files::findOne($request -> get('f'));

        if ($file instanceof Files && is_file($file->path)) {

            $answer = Yii::$app->response->sendFile($file->path);

        } else {

            $answer = self::FILE_NOT_FOUND_MESSAGE;
        }

        return $answer;
    }
}
